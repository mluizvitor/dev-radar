import React from 'react';
import './styles.css';
import Icon from '@mdi/react';
import { mdiPencilOutline, mdiTrashCanOutline } from '@mdi/js';

function CardDev({ dev }) {
  return (
    <li className="dev-item">
      <div className="body">
        <header>
          <img src={dev.avatar_url} alt={dev.name} />
          <div className="user-info">
            <strong>{dev.name}</strong>
            <span>{dev.techs ? dev.techs.join(', ') : ''}</span>
          </div>
        </header>
        <p>{dev.bio}</p>
      </div>
      <div className="footer">
        <a href={`https://github.com/${dev.github_username}`}>
          Acessar perfil no github
        </a>
        <button type="button">
          <Icon path={mdiPencilOutline} color="#F095A9" />
        </button>
        <button type="button">
          <Icon path={mdiTrashCanOutline} color="#F095A9" />
        </button>
      </div>
    </li>
  );
}

export default CardDev;
