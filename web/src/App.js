import React, { useState, useEffect } from 'react';
import api from './services/api';

import CardDev from './components/CardDev';
import FormDev from './components/FormDev';

import './global.css';
import './App.css';
import './Sidebar.css';
import './Main.css';

function App() {
  const [devs, setDevs] = useState([]);

  useEffect(() => {
    async function loadDevs() {
      const response = await api.get('/devs');

      setDevs(response.data);
    }
    loadDevs();
  }, []);

  async function handleAddDev(data) {
    const response = await api.post('/devs', data);

    setDevs([...devs, response.data]);
  }

  async function handleDeleteDev(data) {
    const response = await api.delete(`/devs/`, data);

    setDevs([...devs].splice(response.data));
  }

  return (
    <div id="app">
      <aside>
        <strong>Cadastrar</strong>
        <FormDev onSubmit={handleAddDev} />
      </aside>

      <main>
        <ul>
          {devs.map(dev => (
            <CardDev key={dev._id} dev={dev} />
          ))}
        </ul>
      </main>
    </div>
  );
}

export default App;
