import React, { useEffect, useState } from "react";
import MapView, { Marker, Callout } from "react-native-maps";
import {
  StyleSheet,
  Image,
  View,
  Text,
  TextInput,
  TouchableOpacity
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import {
  requestPermissionsAsync,
  getCurrentPositionAsync
} from "expo-location";
import MapStyle from "../utils/MapStyle";

import api from "../services/api";
import { connect, disconnect, subscribeToNewDevs } from "../services/socket";

function Main({ navigation }) {
  const [devs, setDevs] = useState([]);
  const [currentRegion, setCurrentRegion] = useState(null);
  const [techs, setTechs] = useState("ReactJS");

  useEffect(() => {
    async function loadInitialPosition() {
      const { granted } = await requestPermissionsAsync();

      if (granted) {
        const { coords } = await getCurrentPositionAsync({
          enableHighAccuracy: true
        });

        const { latitude, longitude } = coords;
        setCurrentRegion({
          latitude,
          longitude,
          latitudeDelta: 0.03,
          longitudeDelta: 0.03
        });
      }
    }

    loadInitialPosition();
  }, []);

  useEffect(() => {
    subscribeToNewDevs(dev => setDevs([...devs, dev]));
  }, [devs]);

  function setupWebSocket() {
    disconnect();

    const { latitude, longitude } = currentRegion;

    connect(latitude, longitude, techs);
  }

  async function loadDevs() {
    const { latitude, longitude } = currentRegion;

    const response = await api.get("/search", {
      params: {
        latitude,
        longitude,
        techs
      }
    });

    setDevs(response.data);
    setupWebSocket();
  }

  function handleRegionChange(region) {
    setCurrentRegion(region);
  }

  if (!currentRegion) {
    return null;
  }

  return (
    <>
      <MapView
        onRegionChangeComplete={handleRegionChange}
        initialRegion={currentRegion}
        style={styles.map}
        customMapStyle={MapStyle}
      >
        {devs.map(dev => (
          <Marker
            key={dev._id}
            coordinate={{
              latitude: dev.location.coordinates[1],
              longitude: dev.location.coordinates[0]
            }}
          >
            <Image
              style={styles.avatar}
              source={{
                uri: dev.avatar_url
              }}
            />
            <Callout
              onPress={() => {
                navigation.navigate("Profile", {
                  github_username: dev.github_username
                });
              }}
            >
              <View style={styles.callout}>
                <Text style={styles.devName}>{dev.name}</Text>
                <Text style={styles.devTechs}>{dev.techs.join(", ")}</Text>
                <Text style={styles.devBio}>{dev.bio}</Text>
              </View>
            </Callout>
          </Marker>
        ))}
      </MapView>
      <View style={styles.searchForm}>
        <TextInput
          style={styles.searchBar}
          placeholder="Buscar devs por techs"
          placeholderTextColor="#999"
          autoCapitalize="words"
          autoCorrect={false}
          value={techs}
          onChangeText={setTechs}
        />
        <TouchableOpacity onPress={loadDevs} style={styles.searchButton}>
          <MaterialCommunityIcons name="magnify" color="#FFF" size={24} />
        </TouchableOpacity>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  map: {
    flex: 1
  },
  avatar: {
    width: 56,
    height: 56,
    borderColor: "#FFF",
    borderWidth: 4,
    borderRadius: 8
  },
  callout: {
    width: 256
  },
  devName: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#2C162F"
  },
  devBio: {
    fontSize: 16,
    paddingVertical: 8,
    color: "#2C162F",
    opacity: 0.7
  },
  devTechs: {
    fontSize: 12,
    color: "#F095A9"
  },
  searchForm: {
    position: "absolute",
    top: 0,
    paddingHorizontal: 16,
    paddingVertical: 16,
    flexDirection: "row"
  },
  searchBar: {
    flex: 1,
    fontSize: 16,
    backgroundColor: "#FFF",
    height: 48,
    borderRadius: 24,
    paddingHorizontal: 16,
    elevation: 4
  },
  searchButton: {
    width: 48,
    height: 48,
    backgroundColor: "#2C162F",
    borderRadius: 24,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 8,
    elevation: 4
  }
});

export default Main;
