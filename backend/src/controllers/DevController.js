import axios from 'axios';
import Dev from '../models/Dev';
import parseStringAsArray from '../utils/parseStringAsArray';
import { findConnections, sendMessage } from '../websocket';

class DevController {
  // Index
  async index(req, res) {
    const devs = await Dev.find();

    return res.json(devs);
  } // Fim do Index

  // Store
  async store(req, res) {
    const { github_username, techs, latitude, longitude } = req.body;

    const devExists = await Dev.findOne({ github_username });

    if (devExists) {
      return res.status(400).json({ error: 'Usuário já cadastrado' });
    }

    const apiResponse = await axios.get(
      `https://api.github.com/users/${github_username}`
    );
    const { login, name = login, bio, avatar_url } = apiResponse.data;
    const techsArray = parseStringAsArray(techs);
    const location = {
      type: 'Point',
      coordinates: [longitude, latitude],
    };

    const dev = await Dev.create({
      name,
      github_username,
      bio,
      avatar_url,
      techs: techsArray,
      location,
    });

    const sendSocketMessageTo = findConnections(
      { latitude, longitude },
      techsArray
    );

    sendMessage(sendSocketMessageTo, 'newDev', dev);

    return res.json(dev);
  } // Fim do Store

  async update(req, res) {
    const { username } = req.params;

    const devExists = await Dev.findOne({ github_username: username });

    if (!devExists) {
      return res.status(400).json({ error: 'Dev não encontrado' });
    }

    try {
      const techsArray = parseStringAsArray(req.body.techs);

      req.body.techs = techsArray;

      delete req.body.github_username;

      await Dev.findOneAndUpdate({ github_username: username }, req.body, {
        useFindAndModify: false,
        upsert: true,
        new: true,
      });

      return res.json(await Dev.findOne({ github_username: username }));
    } catch (err) {
      return res.status(400).json({ error: err });
    }
  }

  // Delete
  async delete(req, res) {
    const { username } = req.params;

    const dev = await Dev.findOne({ github_username: username });
    if (!dev) {
      return res.status(400).json({ error: 'Usuário não cadastrado' });
    }

    await dev.delete();

    return res.status(200).json({ ok: true });
  }
}

export default new DevController();
