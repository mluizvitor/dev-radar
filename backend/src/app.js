import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import http from 'http';
import { setupWebSocket } from './websocket';

import routes from './routes';

const app = express();
const server = http.Server(app);

setupWebSocket(server);

mongoose.connect(
  'mongodb+srv://mluizvitor:omnistack@cluster0-anuva.mongodb.net/week10?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

app.use(cors());
app.use(express.json());
app.use(routes);

export default server;
